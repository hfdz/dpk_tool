//
//  DBaseTool.h
//  DPK
//
//  Created by rt on 2019/3/1.
//  Copyright © 2019 rt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCalculationTool : NSObject

/*****    DCalculationTool 解决数据计算精度丢失问题 ，核心类（NSDecimalNumber）   **********/

/*
 * 加法计算
 * firstNumber 第一位
 * nextNumber  第二位
 * 返回字符串的结果
 */

+(NSString*)additionActionWithFirstNumber:(NSString*)firstNumber withNextNumber:(NSString*)nextNumber;

/*
 * 减法计算
 * firstNumber 第一位
 * nextNumber  第二位
 * 返回字符串的结果
 */

+(NSString*)subtractionActionWithFirstNumber:(NSString*)firstNumber withNextNumber:(NSString*)nextNumber;

/*
 * 乘法计算
 * firstNumber 第一位
 * nextNumber  第二位
 * 返回字符串的结果
 */

+(NSString*)multiplicationActionWithFirstNumber:(NSString*)firstNumber withNextNumber:(NSString*)nextNumber;

/*
 * 除法计算
 * firstNumber 第一位
 * nextNumber  第二位
 * 返回字符串的结果
 */

+(NSString*)divisionActionWithFirstNumber:(NSString*)firstNumber withNextNumber:(NSString*)nextNumber;

@end


