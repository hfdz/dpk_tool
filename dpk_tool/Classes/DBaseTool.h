//
//  DBaseTool.h
//  DPK
//
//  Created by rt on 2019/3/1.
//  Copyright © 2019 rt. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DBaseTool : NSObject

/*
 * 隐藏手机号的中间数字
 * phoneString 手机号
 */

+ (NSString *) hidenPhoneCenterString:(NSString*) phoneString;

/*
 * 检测字符串是否是身份证编号
 * idCardString
 */

+ (BOOL) checkIDcard:(NSString*) idCardString;

/*
 * 银行卡号的检测
 * bankCardString 卡号字符串
 */

+ (BOOL) checkBankIdCard:(NSString*) bankCardString;

/*
 * 剔除卡号里的非法字符
 */

+(NSString *) getDigitsOnly:(NSString*) bankCardString;

/*
 * 返回商店的版本号
 * appID appstoreconnect对应的ID
 */

+ (NSString *) backAppStoreVersion:(NSString*) appID;

/*
 * 返回程序的本地版本号
 */

+ (NSString *) backLocalVersion;

/*
 * 依据APPID 判断是否更新APP
 * appID appstoreconnect对应的ID
 */

+ (BOOL) checkUploadApp:(NSString *)appId;

/*
 * 字典数据转json字符串
 */

+ (NSString *) dictoryChangeToJsonString:(NSMutableDictionary *) dictory;

/*
 * 数组数据转json字符串
 */

+ (NSString *) arrayChangeToJsonString:(NSMutableArray *) array;

/*
 * json字符串转字典
 */

+ (NSDictionary *) jsonStringChangeToDictory:(NSString *) jsonString;

/*
 * json字符串转数组
 */

+ (NSArray *) jsonStringChangeToArray:(NSString *) jsonString;

@end


