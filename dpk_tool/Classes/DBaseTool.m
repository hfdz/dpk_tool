//
//  DBaseTool.m
//  DPK
//
//  Created by rt on 2019/3/1.
//  Copyright © 2019 rt. All rights reserved.
//

#import "DBaseTool.h"

@implementation DBaseTool

//隐藏手机号的中间数字

+ (NSString *) hidenPhoneCenterString:(NSString*) phoneString {
    
    NSString *phoneNew = [[NSString alloc] init];
    
    for (int i = 0; i < phoneString.length; i++) {
        
        NSString *str = [[NSString alloc] init];
        
        if (!(i > 2 && i < 7)) {
            
            str = [phoneString substringWithRange:NSMakeRange(i, 1)];
        }else{
            str = @"*";
        }
        
        phoneNew = [phoneNew stringByAppendingString:str];
        
    }
    
    return phoneNew;
}

//身份证号检测

+ (BOOL) checkIDcard:(NSString*) idCardString {
    
    BOOL flag;
    if (idCardString.length <= 0) {
        flag = NO;
        return flag;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:idCardString];
}

//银行卡号的检测

+ (BOOL) checkBankIdCard:(NSString*) bankCardString{
    
    NSString *digitsOnly = [self getDigitsOnly:bankCardString];
    int sum = 0;
    int digit = 0;
    int addend = 0;
    BOOL timesTwo = false;
    for (int i = (int)(digitsOnly.length - 1); i >= 0; i--)
    {
        digit = [digitsOnly characterAtIndex:i] - '0';
        if (timesTwo)
        {
            addend = digit * 2;
            if (addend > 9) {
                addend -= 9;
            }
        }
        else {
            addend = digit;
        }
        sum += addend;
        timesTwo = !timesTwo;
    }
    int modulus = sum % 10;
    return modulus == 0;
    
}

//剔除卡号里的非法字符

+(NSString *) getDigitsOnly:(NSString*) bankCardString
{
    NSString *digitsOnly = @"";
    char c;
    for (int  i = 0; i < bankCardString.length; i++)
    {
        c = [bankCardString characterAtIndex:i];
        if (isdigit(c))
        {
            digitsOnly =[digitsOnly stringByAppendingFormat:@"%c",c];
        }
    }
    return digitsOnly;
}


+ (NSString *) backAppStoreVersion:(NSString*) appID{
    
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *nowVersion = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=%@",appID]];
    
    NSData *data=[NSData dataWithContentsOfURL:url];
    
    NSError *error2;
    
    if (data==nil) {
        return nil;
    }
    
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error2];
    
    if (jsonDic == nil) {
        return nil;
    }
    NSArray *arrd=[jsonDic objectForKey:@"results"];
    if (arrd==nil) {
        return nil;
    }
    if (arrd.count<=0) {
        return nil;
    }
    
    NSDictionary *dicd=[arrd objectAtIndex:0];
    if (dicd==nil) {
        return nil;
    }
    NSString *newVersion=[dicd objectForKey:@"version"];
    if (newVersion==nil) {
        return nil;
    }
    
    if ([newVersion caseInsensitiveCompare:nowVersion]==NSOrderedDescending) {
        
        return newVersion;
    }
    return nil;
    
}

+ (NSString *) backLocalVersion{
    
     return  [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}


+ (BOOL) checkUploadApp:(NSString *)appId{
    
    NSString * appStoreVersion = [self backAppStoreVersion:appId];
    
    NSString *version = [self backLocalVersion];
    
    //商店版本高于本地版本
    if (([version compare:appStoreVersion options:NSNumericSearch] == NSOrderedAscending)){
        return YES;
    }else{
        return NO;
    }
}

+ (NSString *) dictoryChangeToJsonString:(NSMutableDictionary *)dictory{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictory
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString;
    
    if (!jsonData) {
    }else{
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    NSRange range = {0,jsonString.length};
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    NSRange range2 = {0,mutStr.length};
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    return mutStr;
}

+ (NSString *) arrayChangeToJsonString:(NSMutableArray *) array{
    
    NSData *data=[NSJSONSerialization dataWithJSONObject:array
                                                 options:NSJSONWritingPrettyPrinted
                                                   error:nil];
    if (data == nil) {
        
        return nil;
        
    }
    NSString *str=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    return str;
}

+ (NSDictionary *) jsonStringChangeToDictory:(NSString *) jsonString{
    
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        return nil;
    }
    return dic;
    
}

+ (NSArray *) jsonStringChangeToArray:(NSString *) jsonString{
    
    if (jsonString.length) {
       
        id tmp = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                                 options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers
                                                   error:nil];
        if (tmp) {
            if ([tmp isKindOfClass:[NSArray class]]) {
                
                return tmp;
                
            } else if([tmp isKindOfClass:[NSString class]]
                      || [tmp isKindOfClass:[NSDictionary class]]) {
                
                return [NSArray arrayWithObject:tmp];
                
            } else {
                return nil;
            }
        } else {
            return nil;
        }
        
    } else {
        return nil;
    }
}



@end
