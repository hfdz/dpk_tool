//
//  DBaseTool.m
//  DPK
//
//  Created by rt on 2019/3/1.
//  Copyright © 2019 rt. All rights reserved.
//

#import "DCalculationTool.h"

@implementation DCalculationTool
//加法
+(NSString*)additionActionWithFirstNumber:(NSString*)firstNumber withNextNumber:(NSString*)nextNumber{
    
     NSDecimalNumber *first = [NSDecimalNumber decimalNumberWithString:firstNumber];
     NSDecimalNumber *next = [NSDecimalNumber decimalNumberWithString:nextNumber];
     NSDecimalNumber *result = [first decimalNumberByAdding:next];
    return [NSString stringWithFormat:@"%@",result];
    
}
//减法
+(NSString*)subtractionActionWithFirstNumber:(NSString*)firstNumber withNextNumber:(NSString*)nextNumber{
    
    NSDecimalNumber *first = [NSDecimalNumber decimalNumberWithString:firstNumber];
    NSDecimalNumber *next = [NSDecimalNumber decimalNumberWithString:nextNumber];
    NSDecimalNumber *result = [first decimalNumberBySubtracting:next];
    return [NSString stringWithFormat:@"%@",result];
}
//乘法
+(NSString*)multiplicationActionWithFirstNumber:(NSString*)firstNumber withNextNumber:(NSString*)nextNumber{
    
    NSDecimalNumber *first = [NSDecimalNumber decimalNumberWithString:firstNumber];
    NSDecimalNumber *next = [NSDecimalNumber decimalNumberWithString:nextNumber];
    NSDecimalNumber *result = [first decimalNumberByMultiplyingBy:next];
    return [NSString stringWithFormat:@"%@",result];
    
}

//除法
+(NSString*)divisionActionWithFirstNumber:(NSString*)firstNumber withNextNumber:(NSString*)nextNumber{
    
    NSDecimalNumber *first = [NSDecimalNumber decimalNumberWithString:firstNumber];
    NSDecimalNumber *next = [NSDecimalNumber decimalNumberWithString:nextNumber];
    if ( [[NSString stringWithFormat:@"%@",next] floatValue] == 0) {
        return @"0";
    }
    NSDecimalNumber *result = [first decimalNumberByDividingBy:next];
    return [NSString stringWithFormat:@"%@",result];
    
}


@end
