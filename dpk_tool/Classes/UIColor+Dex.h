//
//  UIColor+Hex.h
//  Godofwealth
//
//  Created by yc on 15/11/30.
//  Copyright © 2015年 wghl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Dex)
+ (UIColor *)colorWithHexString:(NSString *)color;

//从十六进制字符串获取颜色，
//color:支持@“#123456”、 @“0X123456”、 @“123456”三种格式
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;


+(UIImage*) createImageWithColor:(UIColor*) color;
@end
